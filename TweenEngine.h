// TweenEngine.h
#pragma once
#include <memory>
#include <functional>
#include <vector>

#include "Tween.h"
#include "TweenBaseInfo.h"

class TweenEngine
{
public:

	TweenEngine();
	virtual ~TweenEngine();
	
	
	std::shared_ptr< Tween > newTween(
		const float& duration, std::shared_ptr< TweenBaseInfo > = nullptr, std::function<void(TweenBaseInfo*, const float&)> func = nullptr,
		std::function<void(float&) > = Tween::LINEAR );
		

	void update(float dt);
	void clear();

	unsigned int getTweenCount();

private:
	
	std::vector< std::shared_ptr< Tween > > tweens_;
};

// Endof TweenEngine.h
