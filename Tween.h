// Tween.h

#pragma once



#include <functional>
#include <vector>
#include <memory>

#include "TweenBaseInfo.h"

class Tween
{
public:
	enum finish_enums { FINISHED, STOPPED };
	
	static void LINEAR(float& t) {}
	static void CUBIC(float& t) { t *= t; }
	
	static void MOVER(TweenBaseInfo*, const float& );

	Tween(const float& duration, std::shared_ptr< TweenBaseInfo > info, 
		std::function<void(TweenBaseInfo*, const float&) >,
		std::function<void(float& ) > = Tween::LINEAR);
	virtual ~Tween();

	void update(float dt);

	//void addValue(float f, float endValue);
	void start();
	void stop();

	std::shared_ptr<Tween > newParallel(const float& duration, std::shared_ptr<TweenBaseInfo> info = nullptr,
		std::function<void(TweenBaseInfo*, const float&)> = nullptr, std::function<void(float&) > = Tween::LINEAR);

	std::shared_ptr<Tween > newSequence(const float& duration, std::shared_ptr<TweenBaseInfo> info = nullptr,
		std::function<void(TweenBaseInfo*, const float&)> = nullptr, std::function<void(float&) > = Tween::LINEAR);

	void setCallBack(std::function<void(int)> );
	bool isFinished();
private:
	std::function<void(int)> callbackFunc_;
	std::function<void(TweenBaseInfo*, const float& )> traverserFunc_;
	//std::vector < float > startVals_;
	//std::vector < float > endVals_;
	std::shared_ptr<TweenBaseInfo> tweenInfo_;

	float time_ = 0.f;
	float endTime_ = 0.f;

	bool running_ = false;
	bool finished_ = false;
	bool started_ = false;

	std::vector<std::shared_ptr<Tween> > parallel_ ;
	std::shared_ptr<Tween > sequence_;

	std::function<void(float&) > easeFunc_ = Tween::LINEAR;
};

// End of Tween.h

