
// TweenEngine.cpp

#include "TweenEngine.h"


TweenEngine::TweenEngine()
{
}


TweenEngine::~TweenEngine()
{
}

void TweenEngine::update( float dt )
{

	for( unsigned int i = 0; i < tweens_.size(); ++i )
	{
		tweens_[i]->update(dt);
		if (tweens_[i]->isFinished()) {
			tweens_.erase(tweens_.begin() + i);
			--i;
		}

	}
} 
void TweenEngine::clear(){
	tweens_.clear();
}
unsigned int TweenEngine::getTweenCount() {
	return tweens_.size();
}


std::shared_ptr<Tween> TweenEngine::newTween( 
	const float& duration, std::shared_ptr<TweenBaseInfo > info,
		std::function<void( TweenBaseInfo*,const float& )> func,
		std::function<void(float&) > funcEaseType ) {

	auto tween = std::make_shared<Tween>(Tween(duration, info, func, funcEaseType));
	tweens_.push_back(tween);
	return tween;
}
// End of TweenEngine.cpp