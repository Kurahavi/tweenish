#pragma once
#include <functional>
#include <memory>
#include <Urho3D/Math/Vector3.h>
#include <Urho3D/Scene/Node.h>

#include <Urho3D/Math/MathDefs.h>
#include <Urho3D/Math/Vector3.h>

#include "TweenBaseInfo.h"
#include "TweenInfoCustom.h"

namespace Urho3D {

	struct PointRotRadius {
		PointRotRadius(Vector3 v, float ang, float rad) : v_(v), ang_(ang), radius_(rad) {}
		
		Vector3 v_;
		float ang_;
		float radius_;
	};

	template< typename T, typename T2 >
	void TweenMover(TweenBaseInfo* baseInfo, const float& t){
		auto info = (TweenInfoFromTo<T, T2>*) baseInfo;
		auto pos = T2((1.f - t) * info->from_ + t * info->to_);

		info->obj_->SetPosition(pos);
	}

	template< typename T >
	void TweenCircularMovement(TweenBaseInfo* baseInfo, const float& t){
		auto info = (TweenInfoFromTo<T, PointRotRadius>*) baseInfo;

		float ang = (1.f-t) * (info->from_.ang_) + t * ( info->to_.ang_) ;
		float rad = (1.f-t) * (info->from_.radius_) + t * (info->to_.radius_);
		
		auto pos = Vector3( (1.f - t) * info->from_.v_ + t * info->to_.v_ ) ;
		pos += Vector3( cos(ang) * rad, 0.f, sin(ang) * rad);

		info->obj_->SetPosition(pos);
	}


};

