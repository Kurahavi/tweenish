#pragma once

#include <memory>

#include <Urho3D/Math/Vector3.h>

#include "TweenBaseInfo.h"

template< typename T, typename T2 >
struct TweenInfoFromTo : TweenBaseInfo
{
	TweenInfoFromTo(T* obj, T2 from, T2 to) : obj_(obj), from_(from), to_(to){}
	virtual ~TweenInfoFromTo(){}

	T* obj_;
	T2 from_;
	T2 to_;
};

