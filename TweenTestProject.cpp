//
// Copyright (c) 2008-2015 the Urho3D project.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include <Urho3D/Urho3D.h>

#include <Urho3D/Graphics/Animation.h>
#include <Urho3D/Graphics/AnimatedModel.h>
#include <Urho3D/Graphics/AnimationState.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Graphics/DebugRenderer.h>
#include <Urho3D/Graphics/DecalSet.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/UI/Font.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/Graphics/Light.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/UI/Text.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/Graphics/Zone.h>

#include "TweenTestProject.h"

#include <Urho3D/DebugNew.h>

#include <Urho3D/IO/Log.h>

#include <Urho3D/Graphics/Texture2D.h>
DEFINE_APPLICATION_MAIN(TweenTestProject)

#include <memory>
#include "TweenEngine.h"
#include "Tween.h"
#include "TweenBaseInfo.h"
#include "TweenInfoCustom.h"
#include "TweenFunctions.h"

const int CIRCLES = 10;
const int ROWS = 30;
const int COLS = 30;
const int BLOCKS = ROWS * COLS;

TweenTestProject::TweenTestProject(Context* context) :
    Sample(context),
    drawDebug_(false),
	decalCount_(0)
{
}

void TweenTestProject::Start()
{
	// Execute base class startup
    Sample::Start();

	tweenEngine_ = std::make_shared<TweenEngine>(TweenEngine());


    // Create the scene content
    CreateScene();

    // Create the UI content
    CreateInstructions();

    // Setup the viewport for displaying the scene
    SetupViewport();

    // Hook up to the frame update and render post-update events
    SubscribeToEvents();
}

void TweenTestProject::Setup()
{
	Sample::Setup();
	engineParameters_["WindowResizable"] = true;
	
}

void TweenTestProject::CreateScene()
{

	ResourceCache* cache = GetSubsystem<ResourceCache>();

	scene_ = new Scene(context_);

	// Create octree, use default volume (-1000, -1000, -1000) to (1000, 1000, 1000)
	// Also create a DebugRenderer component so that we can draw debug geometry
	scene_->CreateComponent<Octree>();
	scene_->CreateComponent<DebugRenderer>();

	// Create scene node & StaticModel component for showing a static plane
	Node* planeNode = scene_->CreateChild("Plane");
	planeNode->SetScale(Vector3(100.0f, 1.0f, 100.0f));
	StaticModel* planeObject = planeNode->CreateComponent<StaticModel>();
	planeObject->SetModel(cache->GetResource<Model>("Models/Plane.mdl"));
	planeObject->SetMaterial(cache->GetResource<Material>("Materials/StoneTiled.xml"));

	// Create a Zone component for ambient lighting & fog control
	Node* zoneNode = scene_->CreateChild("Zone");
	Zone* zone = zoneNode->CreateComponent<Zone>();
	zone->SetBoundingBox(BoundingBox(-1000.0f, 1000.0f));
	zone->SetAmbientColor(Color(0.15f, 0.15f, 0.15f));
	zone->SetFogColor(Color(0.5f, 0.5f, 0.7f));
	zone->SetFogStart(100.0f);
	zone->SetFogEnd(300.0f);

	// Create a directional light to the world. Enable cascaded shadows on it
	Node* lightNode = scene_->CreateChild("DirectionalLight");
	lightNode->SetDirection(Vector3(0.6f, -1.0f, 0.8f));
	Light* light = lightNode->CreateComponent<Light>();
	light->SetLightType(LIGHT_DIRECTIONAL);
	light->SetCastShadows(true);
	light->SetShadowBias(BiasParameters(0.00025f, 0.5f));

	// Set cascade splits at 10, 50 and 200 world units, fade shadows out at 80% of maximum shadow distance
	light->SetShadowCascade(CascadeParameters(10.0f, 50.0f, 200.0f, 0.0f, 0.8f));

	
	// Using some random material to create image that we can use for our painted texture
	auto mat = cache->GetResource<Material>( "Materials/UrhoDecal.xml");

	auto tex = SharedPtr<Texture2D>( new Texture2D(context_));
	tex->SetSize(0, 0, 0, TEXTURE_DYNAMIC);

	mat->SetTexture(TextureUnit(), tex);


	auto img = SharedPtr<Image>(new Image(context_));

	img->SetSize(512, 512, 4);
	img->Clear(Color(1, 1, 1, 1));
	for (int y = 0; y < 128; ++y)
	{
		int x = 0;
		for (; x < 128; ++x) 
			img->SetPixel(x, y, Color((float)rand() / RAND_MAX, (float)rand() / RAND_MAX, 
				(float)rand() / RAND_MAX, (float)rand() / RAND_MAX));
		

		for (; x < 256; ++x)
			img->SetPixel(x, y, Color(0.5f, 0.f, 0.f, 1.f ));
		
		for (; x < 384; ++x)
			img->SetPixel(x, y, Color(0.0f, 0.5f, 0.f, 1.f));

		for (; x < 512; ++x)
			img->SetPixel(x, y, Color(0.3f, 0.3f, 0.8f, 1.f));
	}
	tex->SetData(img, false);
	
    // Create the camera. Limit far clip distance to match the fog
    cameraNode_ = scene_->CreateChild("Camera");
    Camera* camera = cameraNode_->CreateComponent<Camera>();
    camera->SetFarClip(300.0f);

    // Set an initial position for the camera scene node above the plane
    cameraNode_->SetPosition(Vector3(-5.0f, 10.0f, -5.0f));


	// Create blocks
	for (unsigned int i = 0; i < BLOCKS; ++i) {
		Node* boxNode = scene_->CreateChild("BoxInAir" + String(i)) ;
		boxNode->SetPosition(Vector3(i % COLS, 1.f, i / COLS));
		StaticModel* boxObject = boxNode->CreateComponent<StaticModel>();
		boxObject->SetModel(cache->GetResource<Model>("Models/Box.mdl"));
		boxObject->SetMaterial(cache->GetResource<Material>("Materials/StoneEnvMapSmall.xml"));
		boxObject->SetCastShadows(true);
	}

	startTween();

}



void TweenTestProject::startTween()
{

	ResourceCache* cache = GetSubsystem<ResourceCache>();

	// Waiting 1 second before moving to next sequence
	std::shared_ptr<Tween> baseTween = tweenEngine_->newTween( 1.f );

	// If parallels are shorter than the base they are paralleled to, before moving in sequence
	// the tween waits the whole time.
	auto base2 = baseTween->newSequence(7.f);
	
	// Tween waits until all parallels are done, even if the time is shorter than parallel ones
	auto base3 = base2->newSequence(1.f); 
	

	auto base4 = base3->newSequence(5.f);

	float circleRange = 15.f;
	auto middlePoint = Vector3(0.f, 5.f, 0.f);

	for (unsigned int i = 0; i < BLOCKS; ++i) {
		auto boxNode = scene_->GetChild("BoxInAir" + String(i));
		
		circleRange = 15.f + 5.f * (i%CIRCLES);
		

		float angle = M_PI / BLOCKS * 2.f * i;

		auto startP = Vector3(i % COLS, 1.f, i / COLS );
		auto end1 = Vector3(circleRange * cos(angle), 5.f, circleRange * sin(angle));
		
		base2->newParallel(5.f, 
			std::make_shared< TweenInfoFromTo<Node, Vector3> >(
				TweenInfoFromTo<Node, Vector3>(boxNode,startP, end1)),
			TweenMover<Node, Vector3>, Tween::CUBIC);

		auto endAng = angle + 18 * M_PI * ( (i&CIRCLES) % 2 ? 1 : -1);
		
		base3->newParallel(10.f,
			std::make_shared< TweenInfoFromTo<Node, PointRotRadius> >(
				TweenInfoFromTo<Node, PointRotRadius>(boxNode, PointRotRadius(middlePoint, angle, circleRange),
				PointRotRadius(middlePoint, endAng, circleRange + rand() % 5 - 3 ))), 
			TweenCircularMovement<Node>, Tween::CUBIC);
			

		base4->newParallel(5.f,
			std::make_shared < TweenInfoFromTo<Node, Vector3> >(
				TweenInfoFromTo<Node, Vector3>(boxNode, end1, startP)),
			[i](TweenBaseInfo* baseInfo, const float& t) {
				auto info = (TweenInfoFromTo<Node, Vector3>*) baseInfo;

				auto pos = Vector3((1.f - t) * info->from_ + t * info->to_);
				pos += t*(1.f-t)*(1.f-t) * Vector3(0.f, 30.f + i, 0.f);

				info->obj_->SetPosition(pos);
			}, [](float&t){
				t = sqrt(t);
			});
			
	
	}

	baseTween->setCallBack([this](int finishType) {
		if (finishType == Tween::FINISHED) {
			std::shared_ptr<Tween> baseTween = tweenEngine_->newTween(0.f);

			for (unsigned int i = 0; i < BLOCKS; ++i) {

				auto boxNode = scene_->GetChild("BoxInAir" + String(i));
				int x = i % COLS;
				int z = i/COLS;
				float y = (z % 2 ? 5.f + i : 4.f + i + COLS - x - x);
				auto startP = Vector3(x, 1.f, z);
				auto endP = startP + Vector3(-20.f, y, -20.f);

				baseTween->newParallel(5.f,
					std::make_shared< TweenInfoFromTo<Node, Vector3> >(
					TweenInfoFromTo<Node, Vector3>(boxNode, startP, endP)),
					TweenMover<Node, Vector3>, Tween::LINEAR);
			}

			baseTween->start();
		}
	});

	baseTween->start();

}


void TweenTestProject::PaintDecal()
{
	const float TILE_SIZE = 10.f;

	Vector3 hitPos;
	Drawable* hitDrawable;

	if (Raycast(250.0f, hitPos, hitDrawable))
	{
		// Check if target scene node already has a DecalSet component. If not, create now
		Node* targetNode = hitDrawable->GetNode();
		DecalSet* decal = targetNode->GetComponent<DecalSet>();
		
		if (!decal)
		{
			ResourceCache* cache = GetSubsystem<ResourceCache>();

			decal = targetNode->CreateComponent<DecalSet>();
			decal->SetMaterial(cache->GetResource<Material>("Materials/UrhoDecal.xml"));
		}
		// Add a square decal to the decal set using the geometry of the drawable that was hit, orient it to face the camera,
		// use full texture UV's (0,0) to (1,1). Note that if we create several decals to a large object (such as the ground
		// plane) over a large area using just one DecalSet component, the decals will all be culled as one unit. If that is
		// undesirable, it may be necessary to create more than one DecalSet based on the distance

		Vector2 uvTopLeft = Vector2::ZERO;
		Vector2 uvBotRight = Vector2::ONE;

		int randVal = rand() % 4;
		uvTopLeft = Vector2(randVal / 4.f, 0.f);
		uvBotRight = Vector2(randVal / 4.f + 0.24f, 0.25f);

		++decalCount_;

		if (targetNode->GetName() == "Plane") {
			// because we are drawing these from middle point
			hitPos.x_ = int((hitPos.x_ ) / TILE_SIZE - 0.5f) * TILE_SIZE;
			hitPos.z_ = int((hitPos.z_ ) / TILE_SIZE - 0.5f) * TILE_SIZE;
		}
	
		decal->AddDecal(hitDrawable, hitPos, Quaternion(90.f, 0.f, 0.f), TILE_SIZE, 1.0f, 1.f,
			uvTopLeft, uvBotRight);

	}
}

bool TweenTestProject::Raycast(float maxDistance, Vector3& hitPos, Drawable*& hitDrawable)
{
	hitDrawable = 0;
	
	UI* ui = GetSubsystem<UI>();
	IntVector2 pos = ui->GetCursorPosition();
	// Check the cursor is visible and there is no UI element in front of the cursor

	if (ui->GetElementAt(pos, true))
			return false;

	Graphics* graphics = GetSubsystem<Graphics>();
	Camera* camera = cameraNode_->GetComponent<Camera>();
	Ray cameraRay = camera->GetScreenRay((float)pos.x_ / graphics->GetWidth(), (float)pos.y_ / graphics->GetHeight());
	
	// Pick only geometry objects, not eg. zones or lights, only get the first (closest) hit
	PODVector<RayQueryResult> results;
	RayOctreeQuery query(results, cameraRay, RAY_TRIANGLE, maxDistance, DRAWABLE_GEOMETRY);
	scene_->GetComponent<Octree>()->RaycastSingle(query);
	if (results.Size())
	{
		RayQueryResult& result = results[0];
		hitPos = result.position_;
		hitDrawable = result.drawable_;
		return true;
	}

	return false;
}


void TweenTestProject::CreateInstructions()
{

	ResourceCache* cache = GetSubsystem<ResourceCache>();
	UI* ui = GetSubsystem<UI>();

	// Create a Cursor UI element because we want to be able to hide and show it at will. When hidden, the mouse cursor will
	// control the camera, and when visible, it will point the raycast target
	XMLFile* style = cache->GetResource<XMLFile>("UI/DefaultStyle.xml");




    // Construct new Text object, set string to display and font to use
    instructionText = ui->GetRoot()->CreateChild<Text>();
    instructionText->SetText(
        "Use WASD keys and mouse/touch to move\n"
        "Space to toggle debug geometry\n"
		"New line!\n"
		"Click on ground to draw tile from programatically created texture!"
    );
    instructionText->SetFont(cache->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 15);
	
    // The text has multiple rows. Center them in relation to each other
    instructionText->SetTextAlignment(HA_CENTER);

    // Position the text relative to the screen center
    instructionText->SetHorizontalAlignment(HA_CENTER);
    instructionText->SetVerticalAlignment(VA_CENTER);
    instructionText->SetPosition(0, ui->GetRoot()->GetHeight() / 4);
}

void TweenTestProject::SetupViewport()
{
    Renderer* renderer = GetSubsystem<Renderer>();

    // Set up a viewport to the Renderer subsystem so that the 3D scene can be seen
    SharedPtr<Viewport> viewport(new Viewport(context_, scene_, cameraNode_->GetComponent<Camera>()));
    renderer->SetViewport(0, viewport);
}

void TweenTestProject::SubscribeToEvents()
{
    // Subscribe HandleUpdate() function for processing update events
    SubscribeToEvent(E_UPDATE, HANDLER(TweenTestProject, HandleUpdate));

    // Subscribe HandlePostRenderUpdate() function for processing the post-render update event, sent after Renderer subsystem is
    // done with defining the draw calls for the viewports (but before actually executing them.) We will request debug geometry
    // rendering during that event
    SubscribeToEvent(E_POSTRENDERUPDATE, HANDLER(TweenTestProject, HandlePostRenderUpdate));
}

void TweenTestProject::MoveCamera(float timeStep)
{

	//instructionText->SetText(String(1/timeStep));


	// Right mouse button controls mouse cursor visibility: hide when pressed
	UI* ui = GetSubsystem<UI>();
	Input* input = GetSubsystem<Input>();

	input->SetMouseVisible(!input->GetMouseButtonDown(MOUSEB_RIGHT));

	// Do not move if the UI has a focused element (the console)
	if (ui->GetFocusElement())
		return;


    // Movement speed as world units per second
    const float MOVE_SPEED = 20.0f;
    // Mouse sensitivity as degrees per pixel
    const float MOUSE_SENSITIVITY = 0.1f;

	if (input->GetMouseButtonDown(MOUSEB_RIGHT)) {
		// Use this frame's mouse motion to adjust camera node yaw and pitch. Clamp the pitch between -90 and 90 degrees
		IntVector2 mouseMove = input->GetMouseMove();
		yaw_ += MOUSE_SENSITIVITY * mouseMove.x_;
		pitch_ += MOUSE_SENSITIVITY * mouseMove.y_;
		pitch_ = Clamp(pitch_, -90.0f, 90.0f);
	
		// Construct new orientation for the camera scene node from yaw and pitch. Roll is fixed to zero
	    cameraNode_->SetRotation(Quaternion(pitch_, yaw_, 0.0f));
	}

    // Read WASD keys and move the camera scene node to the corresponding direction if they are pressed
    if (input->GetKeyDown('W'))
        cameraNode_->Translate(Vector3::FORWARD * MOVE_SPEED * timeStep);
    if (input->GetKeyDown('S'))
        cameraNode_->Translate(Vector3::BACK * MOVE_SPEED * timeStep);
    if (input->GetKeyDown('A'))
        cameraNode_->Translate(Vector3::LEFT * MOVE_SPEED * timeStep);
    if (input->GetKeyDown('D'))
        cameraNode_->Translate(Vector3::RIGHT * MOVE_SPEED * timeStep);


	// Paint decal with the left mousebutton; cursor must be visible
	//if (ui->GetCursor()->IsVisible() && input->GetMouseButtonPress(MOUSEB_LEFT))
	
	if (input->GetMouseButtonPress(MOUSEB_LEFT))
			PaintDecal();

    // Toggle debug geometry with space
    if (input->GetKeyPress(KEY_SPACE))
        drawDebug_ = !drawDebug_;
}

void TweenTestProject::HandleUpdate(StringHash eventType, VariantMap& eventData)
{
    using namespace Update;

    // Take the frame time step, which is stored as a float
    float timeStep = eventData[P_TIMESTEP].GetFloat();

	// Update tweens
	tweenEngine_->update(timeStep);

    // Move the camera, scale movement with time step
    MoveCamera(timeStep);
}

void TweenTestProject::HandlePostRenderUpdate(StringHash eventType, VariantMap& eventData)
{
    // If draw debug mode is enabled, draw viewport debug geometry, which will show eg. drawable bounding boxes and skeleton
    // bones. Note that debug geometry has to be separately requested each frame. Disable depth test so that we can see the
    // bones properly
    if (drawDebug_)
        GetSubsystem<Renderer>()->DrawDebugGeometry(false);
}
