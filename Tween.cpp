// Tween.cpp

#include "Tween.h"
#include "TweenEngine.h"

#include <string>
#include <memory>

#include "TweenBaseInfo.h"

Tween::Tween(const float& duration, std::shared_ptr<TweenBaseInfo> info,
	std::function < void(TweenBaseInfo*, const float&)> func, std::function<void(float&) > funcEase ) :
		endTime_(duration), 
		tweenInfo_(info),
		traverserFunc_(func),
		easeFunc_(funcEase),

		time_(0.f),
		started_(false),
		running_(false),
		finished_(false),
		sequence_(NULL)

{
	
}


Tween::~Tween()
{
}

std::shared_ptr<Tween >  Tween::newSequence(const float& duration, std::shared_ptr<TweenBaseInfo> info,
	std::function < void(TweenBaseInfo*, const float&)> func, std::function<void(float&) > funcEase) {
	
	sequence_ = std::make_shared< Tween >( Tween(duration, info, func, funcEase ));
	return sequence_;	
}

std::shared_ptr<Tween >  Tween::newParallel(const float& duration, std::shared_ptr<TweenBaseInfo> info,
	std::function < void(TweenBaseInfo*, const float&)> func, std::function<void(float&) > funcEase) {

	auto parallel = std::make_shared< Tween >(Tween(duration, info, func, funcEase));
	parallel_.push_back(parallel);
	return parallel;
}


void Tween::setCallBack(std::function<void(int)> cb){
	callbackFunc_ = cb;
}


void Tween::start(){
	started_ = true;
	if (time_ <= endTime_) {
		running_ = true;
		for( unsigned int i = 0; i < parallel_.size(); ++i )
		{
			parallel_[i]->start();				
		}
	}
}


void Tween::stop() {
	if (!running_ && callbackFunc_){
		callbackFunc_(Tween::STOPPED);

	}
	finished_ = true;
	running_ = false;
}

void Tween::update(float dt) {
	for (unsigned int i = 0; i < parallel_.size(); ++i) {
		parallel_[i]->update(dt);
		if (parallel_[i]->finished_)
		{
			parallel_[i] = parallel_[ parallel_.size() - 1];
			parallel_.erase(parallel_.end() - 1 );
			--i;
		}
	}
	

	if (!running_) {
		if (time_ < endTime_)
			return;
		if (sequence_ && sequence_->finished_)
			sequence_ = NULL;

		if (sequence_ && !sequence_->started_ && parallel_.size() == 0) {
			sequence_->start();
		}

		if(sequence_ && sequence_->started_ )
			sequence_->update(dt);

		finished_ = (!sequence_ || sequence_->finished_ ) && parallel_.size() == 0;
		if (finished_ && callbackFunc_) {
			callbackFunc_(Tween::FINISHED);
			callbackFunc_ = nullptr;
		}
		return;
	}

	time_ += dt;
	if (time_ >= endTime_)  {
		time_ = endTime_;
		running_ = false;

	}



	if (traverserFunc_ ){
		float t = time_ / endTime_;
		if ( easeFunc_)
			easeFunc_(t);
		traverserFunc_(tweenInfo_.get(), t); 
	}


}


bool Tween::isFinished() {
	return finished_;
}


// End of Tween.cpp
